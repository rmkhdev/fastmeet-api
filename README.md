# Application api server
> Для управления встречами


### Запуск.
При запуске любого скрипта, миграции, команды выполнить `export PYTHONPATH=$(pwd)`


### Деплой.
1. Создать database.env по шаблону database.template.env c параметрами БД.

2. Прописать параметры, при необходимости в app/settings/production.py.  
   
3. docker-compose build.
   
4. docker-compose up -d.
   
5. Для создания пользователя (внутри контейнера приложения) выполнить:
   
   `python application/manage/createsuperuser.py USERNAME PASSWORD`


### Автотесты.
1. Запуск в GITLAB - .gitlab-ci.yml.

2. Перед тестами `export env_test=true`.
   
3. Прописать параметры при необходимости в app/settings/ci.py.
