from aiohttp import web
from application.queries.members import (
    query_get, query_select_by_meeting, query_select_by_member,
    query_insert, query_remove, query_remove_meeting,
)
from application.schemas.members import MembersSchema, MembersCreateSchema
from application.utils.common import get_request_json
from application.utils.authentication import auth_required, session_get
from marshmallow import ValidationError
from asyncpg.exceptions import UniqueViolationError
from application.const.http import (
    HTTP_STATUS_CODE_NOT_FOUND, HTTP_STATUS_CODE_OK,
    HTTP_STATUS_CODE_CREATED, HTTP_STATUS_CODE_BAD
)


routes = web.RouteTableDef()


@routes.get('/api/members')
@auth_required
async def members_meetings(request):
    connection = request.app['db_engine']
    creator_id = await session_get(request)

    # get data
    data = await query_select_by_member(connection=connection, member=creator_id)
    schema = MembersSchema(many=True)

    return web.json_response({
        'data': schema.dump(data)
    })


@routes.get('/api/members/meeting/{id}')
@auth_required
async def members_meeting(request):
    connection = request.app['db_engine']

    # filter
    meeting = int(request.match_info['id'])

    # get data
    data = await query_select_by_meeting(connection=connection, meeting=meeting)
    schema = MembersSchema(many=True)

    return web.json_response({
        'data': schema.dump(data)
    })


@routes.get('/api/members/member/{id}')
@auth_required
async def members_member(request):
    connection = request.app['db_engine']

    # filter
    member = int(request.match_info['id'])

    # get data
    data = await query_select_by_member(connection=connection, member=member)
    schema = MembersSchema(many=True)

    return web.json_response({
        'data': schema.dump(data)
    })


@routes.post('/api/members')
@auth_required
async def members_create(request):
    connection = request.app['db_engine']

    form = await get_request_json(request)

    schema = MembersSchema()
    schema_create = MembersCreateSchema()

    try:
        params = schema_create.load(form)

        # save
        insert = await query_insert(connection=connection, values=params)

        if insert is None:
            return web.json_response({
                'reason': 'Error by create a member',
                'detail': 'fail query_insert'
            }, status=HTTP_STATUS_CODE_BAD)

        # get it
        data = await query_get(connection=connection, id=insert['id'])
        return web.json_response({
            'data': schema.dump(data),
        }, status=HTTP_STATUS_CODE_CREATED)

    except ValidationError as error:
        return web.json_response({
            'reason': 'Error options',
            'detail': error.messages
        }, status=HTTP_STATUS_CODE_BAD)

    except UniqueViolationError as error:
        return web.json_response({
            'reason': 'Duplicate member',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)

    except Exception as error:
        return web.json_response({
            'reason': 'Unknown error',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.get('/api/members/{id}')
@auth_required
async def members_get(request):
    connection = request.app['db_engine']
    id = int(request.match_info['id'])

    # get by id
    data = await query_get(
        connection=connection,
        id=id
    )
    schema = MembersSchema()

    if data is None:
        return web.json_response({
            'reason': 'Member not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    return web.json_response({
        'data': schema.dump(data),
    })


@routes.delete('/api/members/{id}')
@auth_required
async def members_remove(request):
    connection = request.app['db_engine']

    id = int(request.match_info['id'])

    # get
    data = await query_get(connection=connection, id=id)

    if data is None:
        return web.json_response({
            'reason': 'Member not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    # remove
    success = await query_remove(connection=connection, id=id)

    if success:
        return web.json_response({
            'success': success
        }, status=HTTP_STATUS_CODE_OK)

    return web.json_response({
        'reason': 'Error remove a user',
    }, status=HTTP_STATUS_CODE_BAD)


@routes.post('/api/members/meeting/{id}')
@auth_required
async def members_remove_meeting(request):
    connection = request.app['db_engine']

    meeting_id = int(request.match_info['id'])

    form = await get_request_json(request)

    # remove
    success = await query_remove_meeting(connection=connection, meeting_id=meeting_id, user_id=form['user_id'])

    if success:
        return web.json_response({
            'success': success
        }, status=HTTP_STATUS_CODE_OK)

    return web.json_response({
        'reason': 'Error remove a user from meeting',
    }, status=HTTP_STATUS_CODE_BAD)
