from aiohttp import web
from marshmallow import EXCLUDE

from application.queries.meetings import (
    query_insert, query_get_ports, query_get_meeting,
    query_get, query_select_list, query_select_list_by_member,
    query_update, query_remove,
)

from application.queries.ports import query_update as ports_query_update
from application.schemas.meetings import (
    MeetingsSchema, MeetingsFilterSchema, MeetingSchema,
    MeetingCreateSchema, MeetingUpdateSchema, MeetingsListSchema,
)

from application.schemas.ports import PortsUpdateSchema
from application.utils.common import get_request_json
from application.utils.authentication import auth_required, session_get, session_get_user
from marshmallow import ValidationError
from asyncpg.exceptions import UniqueViolationError
from application.utils import datetime_utils
from urllib.parse import unquote
from application.const.http import (
    HTTP_STATUS_CODE_NOT_FOUND, HTTP_STATUS_CODE_OK,
    HTTP_STATUS_CODE_CREATED, HTTP_STATUS_CODE_BAD
)
from application.config import config

routes = web.RouteTableDef()


@routes.get('/api/meetings')
@auth_required
async def meetings_list(request):
    connection = request.app['db_engine']
    creator = await session_get_user(request)

    # convert query params
    query = dict()
    query['name'] = unquote(request.rel_url.query.get('name', ''))
    query['page'] = request.rel_url.query.get('page', -1)
    query['per_page'] = request.rel_url.query.get('per_page', -1)
    query['ports'] = request.rel_url.query.getall('ports', [])
    query['status'] = request.rel_url.query.getall('status', [])

    if creator['level'] != 2:
        query['creator_id'] = creator['id']

    schema_filter = MeetingsFilterSchema()
    filter = schema_filter.load(query)

    try:
        result, total = await query_select_list(
            connection=connection,
            **filter
        )

        schema = MeetingsSchema(many=True)

        return web.json_response({
            'total': total,
            'data': schema.dump(result)
        })

    except Exception as error:
        return web.json_response({
            'reason': 'Error by get meetings',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.get('/api/meetings_my')
@auth_required
async def meetings_list_my(request):
    connection = request.app['db_engine']
    creator = await session_get_user(request)

    member_id = creator['id']

    try:
        result = await query_select_list_by_member(
            connection=connection,
            member_id=member_id
        )

        schema = MeetingsListSchema(many=True)

        return web.json_response({
            'data': schema.dump(result)
        })

    except Exception as error:
        return web.json_response({
            'reason': 'Error by get meetings',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.get('/api/meetings/{id}')
@auth_required
async def meetings_get(request):
    connection = request.app['db_engine']

    id = request.match_info['id']

    try:
        # get meeting
        data = await query_get_meeting(
            connection=connection,
            id=id
        )

        schema = MeetingsSchema()

        if data is None:
            return web.json_response({
                'reason': 'Meeting not found',
            }, status=HTTP_STATUS_CODE_NOT_FOUND)

        return web.json_response({
            'data': schema.dump(data),
            'server': config.MEETING,
        })

    except Exception as error:
        return web.json_response({
            'reason': 'Error by get meeting',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_NOT_FOUND)


@routes.post('/api/meetings')
@auth_required
async def meetings_create(request):
    connection = request.app['db_engine']
    creator_id = await session_get(request)

    data = await get_request_json(request)

    # current date
    data['created_at'] = str(datetime_utils.datetime_utc_tz())
    data['creator_id'] = creator_id
    data['status'] = 0

    schema = MeetingSchema()
    schema_create = MeetingCreateSchema(unknown=EXCLUDE)

    try:
        params = schema_create.load(data)

        # save meeting
        rows = await query_insert(connection=connection, values=params)

        if len(rows):
            meeting = schema.dump(rows[0])
            port_id = meeting.get('port_id')

            # set port is using
            values = PortsUpdateSchema().load(dict(
                using=True
            ), partial=True)

            await ports_query_update(connection=connection, id=int(port_id), values=values)

            # --
            return web.json_response({
                'data': meeting,
            }, status=HTTP_STATUS_CODE_CREATED)

        else:
            return web.json_response({
                'reason': 'Error by create a meeting',
                'detail': 'fail query_insert'
            }, status=HTTP_STATUS_CODE_BAD)

    except ValidationError as error:
        return web.json_response({
            'reason': 'Error options',
            'detail': error.messages
        }, status=HTTP_STATUS_CODE_BAD)

    except UniqueViolationError as error:
        return web.json_response({
            'reason': 'Duplicate meeting',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)

    except Exception as error:
        return web.json_response({
            'reason': 'Unknown error',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.put('/api/meetings/{id}')
@auth_required
async def meetings_update(request):
    connection = request.app['db_engine']

    id = int(request.match_info['id'])
    data = await get_request_json(request)

    if len(data.keys()) == 0:
        return web.json_response({
            'reason': 'Error options',
        }, status=HTTP_STATUS_CODE_BAD)

    schema = MeetingSchema()
    schema_update = MeetingUpdateSchema()

    try:
        params = schema_update.load(data)

        # save meeting
        success = await query_update(connection=connection, id=id, values=params)

        if success:
            data = await query_get(connection=connection, id=id)

            # --
            return web.json_response({
                'data': schema.dump(data),
            }, status=HTTP_STATUS_CODE_OK)

        else:
            return web.json_response({
                'reason': 'Error by update a meeting',
                'detail': 'fail query_update'
            }, status=HTTP_STATUS_CODE_OK)

    except ValidationError as error:
        return web.json_response({
            'reason': 'Error options',
            'detail': error.messages
        }, status=HTTP_STATUS_CODE_OK)

    except Exception as error:
        return web.json_response({
            'reason': 'Unknown error',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_OK)


@routes.delete('/api/meetings/{id}')
@auth_required
async def meetings_remove(request):
    connection = request.app['db_engine']

    id = int(request.match_info['id'])

    # get
    data = await query_get(connection=connection, id=id)

    if data is None:
        return web.json_response({
            'reason': 'Meeting not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    # remove
    success = await query_remove(connection=connection, id=id)

    # take off port
    if success:
        port_id = int(data['port_id'])

        # set port is using
        values = PortsUpdateSchema().load(dict(
            using=False
        ), partial=True)

        success = await ports_query_update(connection=connection, id=port_id, values=values)

        return web.json_response({
            'success': success
        })
    else:
        return web.json_response({
            'reason': 'Error remove a meeting',
        }, status=HTTP_STATUS_CODE_BAD)
