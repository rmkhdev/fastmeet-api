from aiohttp import web
from application.queries.users import query_get_by_username, query_get
from application.schemas.users import UsersSchema
from application.utils.common import get_request_json
from application.utils.password import check_password
from application.utils.authentication import session_set, session_remove, session_get
from application.utils.authentication import auth_required
from application.const.http import (
    HTTP_STATUS_CODE_NOT_FOUND, HTTP_STATUS_CODE_OK, HTTP_STATUS_CODE_UNAUTHORIZED
)


routes = web.RouteTableDef()


@routes.post('/api/auth/login')
async def auth_login(request):
    # props
    connection = request.app['db_engine']
    schema = UsersSchema()

    params = await get_request_json(request)
    username = params['username']
    password = params['password']

    # get user
    data = await query_get_by_username(connection=connection, username=username)

    if data is None:
        return web.json_response({
            'reason': 'User not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    # check password
    success = check_password(
        username=username,
        password=password,
        password_hash_base=data['password'],
    )

    if success:
        # authenticate user
        await session_set(request, data['id'])

        return web.json_response({
            'data': schema.dump(data),
        }, status=HTTP_STATUS_CODE_OK)

    return web.json_response(status=HTTP_STATUS_CODE_UNAUTHORIZED)


@routes.post('/api/auth/logout')
@auth_required
async def auth_logout(request):
    user_id = await session_get(request)

    if user_id is None:
        return web.json_response(status=HTTP_STATUS_CODE_OK)

    await session_remove(request)

    return web.json_response(status=HTTP_STATUS_CODE_OK)


@routes.get('/api/auth/check')
@auth_required
async def auth_check(request):
    connection = request.app['db_engine']
    user_id = await session_get(request)

    # get user by id
    data = await query_get(
        connection=connection,
        id=user_id
    )
    schema = UsersSchema()

    if data is None:
        return web.json_response({
            'reason': 'User not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    return web.json_response({
        'data': schema.dump(data),
    }, status=HTTP_STATUS_CODE_OK)
