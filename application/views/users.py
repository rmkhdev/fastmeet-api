from aiohttp import web
from application.queries.users import query_select, query_insert, query_get, query_update, query_remove
from application.schemas.users import UsersSchema, UsersCreateSchema, UsersUpdateSchema
from application.utils.common import get_request_json
from marshmallow import ValidationError
from asyncpg.exceptions import UniqueViolationError
from application.utils.password import crypt_password
from application.utils.authentication import auth_required
from application.const.http import (
    HTTP_STATUS_CODE_NOT_FOUND, HTTP_STATUS_CODE_OK,
    HTTP_STATUS_CODE_CREATED, HTTP_STATUS_CODE_BAD
)


routes = web.RouteTableDef()


@routes.get('/api/users')
@auth_required
async def users_list(request):
    connection = request.app['db_engine']

    data = await query_select(connection=connection)

    schema = UsersSchema(many=True)

    return web.json_response({
        'data': schema.dump(data)
    })


@routes.post('/api/users')
@auth_required
async def users_create(request):
    connection = request.app['db_engine']

    params = await get_request_json(request)

    schema = UsersSchema()
    schema_create = UsersCreateSchema()

    try:
        values = schema_create.load(params)

        # hashing password
        values['password'] = crypt_password(username=values['username'], password=values['password'])

        # save user
        data = await query_insert(connection=connection, values=values)

        if data is not None:
            return web.json_response({
                'data': schema.dump(data)
            }, status=HTTP_STATUS_CODE_CREATED)

        return web.json_response({
            'reason': 'Error by create a user',
            'detail': 'fail query_insert'
        }, status=HTTP_STATUS_CODE_BAD)

    except ValidationError as error:
        return web.json_response({
            'reason': 'Error options',
            'detail': error.messages
        }, status=HTTP_STATUS_CODE_BAD)

    except UniqueViolationError as error:
        return web.json_response({
            'reason': 'Duplicate user',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)

    except Exception as error:
        return web.json_response({
            'reason': 'Unknown error',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.put('/api/users/{id}')
@auth_required
async def users_update(request):
    connection = request.app['db_engine']

    params = await get_request_json(request)
    id = int(request.match_info['id'])

    # check params on empty
    if len(params.keys()) == 0:
        return web.json_response({
            'reason': 'Error options',
        }, status=HTTP_STATUS_CODE_BAD)

    # get user by id
    data = await query_get(
        connection=connection,
        id=id
    )

    if data is None:
        return web.json_response({
            'reason': 'User not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    schema_create = UsersUpdateSchema()

    try:
        values = schema_create.load(params)
        username = data['username']

        # hashing password
        if 'password' in values and values['password'] is not None:
            values['password'] = crypt_password(username=username, password=values['password'])

        # save user
        success = await query_update(connection=connection, id=id, values=values)
        if success:
            data = await query_get(connection=connection, id=id)
            schema = UsersSchema()

            return web.json_response({
                'data': schema.dump(data)
            }, status=HTTP_STATUS_CODE_OK)

        return web.json_response({
            'reason': 'Error by create a user',
            'detail': 'fail query_insert'
        }, status=HTTP_STATUS_CODE_BAD)

    except ValidationError as error:
        return web.json_response({
            'reason': 'Error options',
            'detail': error.messages
        }, status=HTTP_STATUS_CODE_BAD)

    except UniqueViolationError as error:
        return web.json_response({
            'reason': 'Duplicate user',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)

    except Exception as error:
        return web.json_response({
            'reason': 'Unknown error',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.get('/api/users/{id}')
@auth_required
async def users_get(request):
    connection = request.app['db_engine']
    id = int(request.match_info['id'])

    # get user by id
    data = await query_get(
        connection=connection,
        id=id
    )
    schema = UsersSchema()

    if data is None:
        return web.json_response({
            'reason': 'User not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    return web.json_response({
        'data': schema.dump(data),
    })


@routes.delete('/api/users/{id}')
@auth_required
async def users_remove(request):
    connection = request.app['db_engine']

    id = int(request.match_info['id'])

    # get
    data = await query_get(connection=connection, id=id)

    if data is None:
        return web.json_response({
            'reason': 'User not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    # remove
    success = await query_remove(connection=connection, id=id)

    if success:
        return web.json_response({
            'success': success
        }, status=HTTP_STATUS_CODE_OK)

    return web.json_response({
        'reason': 'Error remove a user',
    }, status=HTTP_STATUS_CODE_BAD)
