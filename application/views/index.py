from aiohttp import web
from application.utils.authentication import auth_required


@auth_required
async def index_view(request):
    return web.Response(body='')
