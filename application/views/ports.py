from aiohttp import web
from application.queries.ports import query_select, query_get, query_insert, query_remove
from application.schemas.ports import PortSchema, PortsRequestSchema, PortsUpdateSchema
from application.utils.authentication import auth_required
from application.utils.common import get_request_json
from marshmallow import ValidationError
from application.const.http import (
    HTTP_STATUS_CODE_NOT_FOUND, HTTP_STATUS_CODE_CREATED,
    HTTP_STATUS_CODE_BAD, HTTP_STATUS_CODE_OK,
)
from asyncpg.exceptions import UniqueViolationError


routes = web.RouteTableDef()


@routes.get('/api/ports')
@auth_required
async def ports_list(request):
    connection = request.app['db_engine']
    params_request = dict()

    # filter
    using = request.rel_url.query.get('using')
    if using is not None:
        params_request['using'] = using

    # order
    order = request.rel_url.query.get('order')
    if order is not None:
        params_request['order'] = order

    # convert
    schema_request = PortsRequestSchema()
    values = schema_request.load(params_request)

    # get data
    data = await query_select(connection=connection, values=values)

    schema = PortSchema(many=True)

    return web.json_response({
        'data': schema.dump(data)
    })


@routes.post('/api/ports')
@auth_required
async def ports_create(request):
    connection = request.app['db_engine']

    params = await get_request_json(request)

    schema = PortSchema()
    schema_create = PortsUpdateSchema()

    try:
        values = schema_create.load(params)

        # save port
        data = await query_insert(connection=connection, values=values)

        if data is not None:
            return web.json_response({
                'data': schema.dump(data)
            }, status=HTTP_STATUS_CODE_CREATED)

        return web.json_response({
            'reason': 'Error by create port',
            'detail': 'fail query_insert'
        }, status=HTTP_STATUS_CODE_BAD)

    except ValidationError as error:
        return web.json_response({
            'reason': 'Error options',
            'detail': error.messages
        }, status=HTTP_STATUS_CODE_BAD)

    except UniqueViolationError as error:
        return web.json_response({
            'reason': 'Duplicate port',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)

    except Exception as error:
        return web.json_response({
            'reason': 'Unknown error',
            'detail': str(error)
        }, status=HTTP_STATUS_CODE_BAD)


@routes.get('/api/ports/{id}')
@auth_required
async def ports_get(request):
    connection = request.app['db_engine']

    id = int(request.match_info['id'])

    # get meeting
    data = await query_get(
        connection=connection,
        id=id
    )

    schema = PortSchema()

    if data is None:
        return web.json_response({
            'reason': 'Port not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    return web.json_response({
        'data': schema.dump(data),
    })


@routes.delete('/api/ports/{id}')
@auth_required
async def ports_remove(request):
    connection = request.app['db_engine']

    id = int(request.match_info['id'])

    # get
    data = await query_get(connection=connection, id=id)

    if data is None:
        return web.json_response({
            'reason': 'Port not found',
        }, status=HTTP_STATUS_CODE_NOT_FOUND)

    # remove
    success = await query_remove(connection=connection, id=id)

    if success:
        return web.json_response({
            'success': success
        }, status=HTTP_STATUS_CODE_OK)

    return web.json_response({
        'reason': 'Error remove port',
    }, status=HTTP_STATUS_CODE_BAD)
