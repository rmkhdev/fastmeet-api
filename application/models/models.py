from sqlalchemy import (
    MetaData, Table, Column,
    SmallInteger, Boolean, String, BigInteger, TIMESTAMP, LargeBinary,
    ForeignKey, UniqueConstraint
)
from .settings import CONVENTION


MetaDataBD = MetaData(naming_convention=CONVENTION)

UsersTable = Table(
    'users',
    MetaDataBD,
    Column('id', BigInteger, primary_key=True, autoincrement=True),
    Column('name', String(length=100), nullable=False),
    Column('username', String(length=100), nullable=False, unique=True),
    Column('password', LargeBinary(), nullable=False),
    Column('level', SmallInteger(), default=0),
)

PortsTable = Table(
    'ports',
    MetaDataBD,
    Column('id', BigInteger, primary_key=True, autoincrement=True),
    Column('port', SmallInteger(), nullable=False, unique=True, index=True),
    Column('using', Boolean, default=False, nullable=False),
)

MeetingTable = Table(
    'meetings',
    MetaDataBD,
    Column('id', BigInteger, primary_key=True, autoincrement=True),
    Column('name', String(length=100)),
    Column(
        'port_id', SmallInteger, ForeignKey(PortsTable.columns.id, ondelete='CASCADE'),
        default=None, unique=True,
    ),
    Column('status', SmallInteger(), default=0, nullable=False),
    Column('created_at', TIMESTAMP(timezone=True), default=None),
    Column(
        'creator_id', BigInteger, ForeignKey(UsersTable.columns.id, ondelete='CASCADE'),
        default=None, nullable=True,
    ),

    UniqueConstraint('name', 'port_id', name='name_port'),
    UniqueConstraint('name', 'creator_id', name='name_creator'),
)

MembersTable = Table(
    'members',
    MetaDataBD,
    Column('id', BigInteger, primary_key=True, autoincrement=True),
    Column(
        'meeting_id', BigInteger, ForeignKey(MeetingTable.columns.id, ondelete='CASCADE'),
        nullable=False,
    ),
    Column(
        'user_id', BigInteger, ForeignKey(UsersTable.columns.id, ondelete='CASCADE'),
        nullable=False,
    ),

    UniqueConstraint('meeting_id', 'user_id', name='meeting_user'),
)
