from asyncpgsa import pg
from application.utils.common import get_engine_url
from application.config import config
from application.queries.users import query_insert
from application.schemas.users import UsersCreateSchema
from application.utils.password import crypt_password
from marshmallow import ValidationError
from asyncpg.exceptions import UniqueViolationError
import asyncio
import argparse


parser = argparse.ArgumentParser(
    prog="Create superuser",
    description="Create superuser for authentification"
)


parser.add_argument(
    "username",
    metavar="username",
    type=str,
    help="login of user",
)

parser.add_argument(
    "password",
    metavar="password",
    type=str,
    help="password of user",
)


async def command(args):
    # connect to bd
    await pg.init(get_engine_url(config.DATABASE))

    # values to save
    params = dict()
    params['name'] = 'System Admin'
    params['username'] = args.username
    params['password'] = args.password
    params['level'] = 3

    # save
    try:
        values = UsersCreateSchema().load(params)

        values['password'] = crypt_password(username=params['username'], password=params['password'])
        result = await query_insert(connection=pg, values=values)

        if result is not None:
            print('Success')

    except ValidationError as error:
        print(f'Validation error: {error.messages}')

    except UniqueViolationError as error:
        print(f'Unique validation error: {error.messages}')

    except Exception as error:
        print(f'Error: {str(error)}')

    # disconnect
    await pg.pool.close()


if __name__ == '__main__':
    arguments = parser.parse_args()
    asyncio.run(command(arguments))
