from application.main import create_application_test
from application.utils.common import get_engine_url
from application.queries.users import query_insert as users_query_insert, query_remove_all as users_query_remove_all
from application.queries.ports import query_insert as ports_query_insert, query_remove_all as ports_query_remove_all
from application.queries.meetings import query_remove_all as meetings_query_remove_all
from application.config import config
from asyncpgsa import pg
from application.utils.password import crypt_password
import pytest


@pytest.fixture
def client(loop, aiohttp_client):
    app = create_application_test()
    cli = loop.run_until_complete(aiohttp_client(app))
    return cli


# run before each test
def pytest_runtest_setup():
    print('')
    print('setup')
    return None


# run after each test
def pytest_runtest_teardown():
    print('')
    print('teardown')
    return None


URLS = dict({
    'login': '/api/auth/login',
    'logout': '/api/auth/logout',
    'users': '/api/users',
})


@pytest.fixture
async def client_auth(client):
    print('')
    print('login')

    data = {
        "username": "test",
        "password": "test",
    }

    # request
    await client.post(URLS['login'], json=data)
    return client


async def logout(client):
    print('')
    print('logout')

    # request
    await client.post(URLS['logout'])


"""
    ADD TEST DATA
"""
DATA_USER = dict({
    "name": "Test Admin",
    "username": "test",
    "password": "test",
    "level": 1
})

DATA_PORTS = [
    {'port': 9040, 'using': False},
    {'port': 9041, 'using': False},
    {'port': 9042, 'using': False},
    {'port': 9043, 'using': False},
    {'port': 9044, 'using': False},
    {'port': 9045, 'using': False},
    {'port': 9046, 'using': False},
    {'port': 9047, 'using': False},
    {'port': 9048, 'using': False},
]


@pytest.fixture(autouse=True)
async def init_user(client):
    print('')
    print('init user')

    await pg.init(get_engine_url(config.DATABASE))

    params = dict({
        'name': DATA_USER['name'],
        'level': DATA_USER['level'],
        'username': DATA_USER['username'],
        'password': DATA_USER['password'],
    })

    # hashing password
    params['password'] = crypt_password(username=params['username'], password=params['password'])

    # clear all users
    await users_query_remove_all(connection=pg)

    # insert new user
    await users_query_insert(connection=pg, values=params)
    print('')


@pytest.fixture(autouse=True)
async def init_ports(client):
    print('')
    print('init ports')

    await pg.init(get_engine_url(config.DATABASE))

    # clear all ports
    await ports_query_remove_all(connection=pg)

    # insert new ports
    for port in DATA_PORTS:
        port = await ports_query_insert(connection=pg, values=port)

    print('')


@pytest.fixture(autouse=True)
async def init_meetings(client):
    print('')
    print('init meetings')

    await pg.init(get_engine_url(config.DATABASE))

    # clear all meetings
    await meetings_query_remove_all(connection=pg)

    print('')
