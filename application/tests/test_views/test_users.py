from application.tests.test_views.conftest import DATA_USER
from application.const.http import HTTP_STATUS_CODE_OK


URLS = dict({
    'login': '/api/auth/login',
    'users': '/api/users',
})


async def test_users_list(client_auth):
    response = await client_auth.get(URLS['users'])
    assert response.status == HTTP_STATUS_CODE_OK

    users = await response.json()
    user_main = users['data'][0]

    assert user_main['name'] == DATA_USER['name']
    assert user_main['username'] == DATA_USER['username']
