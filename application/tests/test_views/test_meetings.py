from application.const.http import HTTP_STATUS_CODE_OK, HTTP_STATUS_CODE_CREATED


URLS = dict({
    'login': '/api/auth/login',
    'create': '/api/meetings',
    'ports': '/api/ports',
})


async def test_meetings_create(client_auth):
    # get ports
    response = await client_auth.get(URLS['ports'], json={'using': True})
    assert response.status == HTTP_STATUS_CODE_OK

    ports = await response.json()
    assert len(ports['data']) > 0

    # create meeting
    if len(ports['data']) > 0:
        port = ports['data'][0]

        # create
        data = {
            "name": "Grumming",
            "port_id": port['id'],
            "status": 2
        }

        response = await client_auth.post(URLS['create'], json=data)
        assert response.status == HTTP_STATUS_CODE_CREATED
