from json import JSONDecodeError
from aiohttp import web
import os


async def get_request_json(request):
    try:
        return await request.json()
    except JSONDecodeError:
        raise web.json_response({
            'result': False,
            'reason': 'Невалидный json'
        }, status=400)


def get_engine_url(params):
    env = bool(os.environ.get('env_test'))

    if env:
        params['NAME'] = params['NAME_TEST']

    return f"postgresql://{params['USER']}:" \
           f"{params['PASSWORD']}@" \
           f"{params['HOST']}/" \
           f"{params['NAME']}"
