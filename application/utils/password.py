"""
Модуль для работы с шифрованием пароля пользователя
"""

import hashlib


def crypt_password(username: str, password: str) -> bytes:
    """
    Зашифровать пароль
    :param username: логин пользователя
    :param password: пароль пользователя
    :return: хеш пароля
    """
    return hashlib.pbkdf2_hmac(
        hash_name='sha256',  # используемый алгоритм хеширования
        password=password.encode('utf-8'),  # конвертирование пароля в байты
        salt=username.encode('utf-8'),  # username в байтах в качестве соли
        iterations=100000,  # итерации в вычислении (рекомендуется не меньше 100 000)
        dklen=128,  # длина ключа вывода
    )


def check_password(username: str, password: str, password_hash_base: str) -> bool:
    """
    Проверка пароля
    :param username: логин пользователя
    :param password: введенный пароль
    :param password_hash_base: зашифровааный пароль
    :return: хеш пароля
    """
    password_hash = hashlib.pbkdf2_hmac(
        hash_name='sha256',  # используемый алгоритм хеширования
        password=password.encode('utf-8'),  # конвертирование пароля в байты
        salt=username.encode('utf-8'),  # username в байтах в качестве соли
        iterations=100000,  # итерации в вычислении (рекомендуется не меньше 100 000)
        dklen=128,  # длина ключа вывода
    )

    if password_hash == password_hash_base:
        return True

    return False
