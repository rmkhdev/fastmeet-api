"""
Модуль для работы с датой
"""

from datetime import datetime, timezone


def datetime_utc():
    """Возврашщает текущую дату со временем в utc формате.
    Например: 2020-10-04 18:47:24.718617"""
    return datetime.now()


def datetime_utc_tz():
    """Возврашщает текущую дату со временем в utc формате с учетом текущей тайм-зоны.
    Например: 2020-10-04 18:47:24.718617+03:00"""
    dt = datetime.now(timezone.utc)
    dt_local = dt.astimezone()
    return dt_local


def datetime_unix():
    """Возврашщает текущую дату со временем в unix формате.
    Например: 1601826444.718617"""
    return datetime.now().timestamp()


def datetime_unix_tz():
    """Возврашщает текущую дату со временем в unix формате с учетом текущей тайм-зоны.
    Например: 1601826444.588"""
    dt = datetime.now(timezone.utc)
    dt_local = dt.astimezone()
    return dt_local.timestamp()
