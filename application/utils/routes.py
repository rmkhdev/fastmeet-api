from aiohttp import web
from application.views import index_view
from application.views import (
    ports_list, ports_get,
    ports_create, ports_remove,
)
from application.views import (
    meetings_list, meetings_list_my, meetings_get,
    meetings_create, meetings_update, meetings_remove,
)
from application.views import (
    users_list, users_get,
    users_create, users_update, users_remove,
)
from application.views import (
    auth_login, auth_logout, auth_check
)
from application.views import (
    members_meeting, members_member, members_get, members_meetings,
    members_create, members_remove, members_remove_meeting,
)


def routes_init(app: web.Application):
    app.add_routes([
        web.get('/', index_view),

        # users
        web.get('/api/users', users_list),
        web.post('/api/users', users_create),
        web.get(r'/api/users/{id:\d+}', users_get),
        web.put(r'/api/users/{id:\d+}', users_update),
        web.delete(r'/api/users/{id:\d+}', users_remove),

        # authentication
        web.post('/api/auth/login', auth_login),
        web.post('/api/auth/logout', auth_logout),
        web.get('/api/auth/check', auth_check),

        # meetings
        web.get('/api/meetings', meetings_list),
        web.get('/api/meetings_my', meetings_list_my),
        web.post('/api/meetings', meetings_create),
        web.get(r'/api/meetings/{id:\d+}', meetings_get),
        web.put(r'/api/meetings/{id:\d+}', meetings_update),
        web.delete(r'/api/meetings/{id:\d+}', meetings_remove),

        # members
        web.get(r'/api/members/meeting/{id:\d+}', members_meeting),
        web.get(r'/api/members/member/{id:\d+}', members_member),
        web.get(r'/api/members/{id:\d+}', members_get),
        web.get('/api/members', members_meetings),
        web.post('/api/members', members_create),
        web.delete(r'/api/members/{id:\d+}', members_remove),
        web.post(r'/api/members/meeting/{id:\d+}', members_remove_meeting),

        # ports
        web.get('/api/ports', ports_list),
        web.post('/api/ports', ports_create),
        web.get(r'/api/ports/{id:\d+}', ports_get),
        web.delete(r'/api/ports/{id:\d+}', ports_remove),
    ])
