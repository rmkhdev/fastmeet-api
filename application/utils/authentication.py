"""
Init session authentication for application
"""

from aiohttp.web import json_response
from cryptography import fernet
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from application.config import config
from aiohttp_session import get_session, new_session
from application.queries.users import query_get
from functools import wraps
import base64


def session_init(app):
    """
    Setup for application authentication session in cookie type
    :param app: it is aiohttp.web.Application()
    """
    key = fernet.Fernet.generate_key()
    secret_key = base64.urlsafe_b64decode(key)

    setup(
        app,
        EncryptedCookieStorage(
            secret_key=secret_key,
            cookie_name=config.AUTH['COOKIE_NAME'],
            path=config.AUTH['PATH'],
            httponly=config.AUTH['HTTPONLY'],
            max_age=config.AUTH['LIFETIME'],
        )
    )


async def session_set(request, user_id):
    """
    Login (set session)
    :param request: request
    :param user_id: PK of user
    """
    session = await new_session(request)
    session['user_id'] = user_id


async def session_remove(request):
    """
    Logout (remove session)
    :param request: request
    """
    session = await get_session(request)
    session.clear()


async def session_get(request):
    """
    Get session variable
    :param request: request
    :return: PK of user
    """
    session = await get_session(request)

    if 'user_id' in session:
        return int(session['user_id'])

    return None


async def session_get_user(request):
    """
    Get session variable
    :param request: request
    :return: PK of user
    """
    session = await get_session(request)

    if 'user_id' in session:
        connection = request.app['db_engine']
        return await query_get(
            connection=connection,
            id=int(session['user_id'])
        )

    return None


def auth_required(func):
    """
    Check login or not
    :param func:
    :return: 403 or run function
    """
    @wraps(func)
    async def wrapper(request):
        session = await session_get(request)

        if session is None:
            return json_response(status=403)

        return await func(request)

    return wrapper
