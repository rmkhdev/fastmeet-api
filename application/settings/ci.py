from .base import BaseSettings


class CiSettings(BaseSettings):
    # WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '80',
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': 'postgres',
        'NAME': 'fastmeetapi',
        'NAME_TEST': 'fastmeetapi',
        'USER': 'fastmeetapi',
        'PASSWORD': 'fastmeetapi',
    }
