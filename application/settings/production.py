from .base import BaseSettings
from application.const.peer import DEBUG_NO_LOGS
import logging


class ProductionSettings(BaseSettings):
    DEBUG = False
    LOGGING_LEVEL = logging.NOTSET

    # WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '80',
    }

    # PEER SERVER PARAMS
    MEETING = {
        'HOST': '192.168.0.190',
        'PATH': '/webrtctunel',
        'SECURE': True,
        'DEBUG': DEBUG_NO_LOGS,
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': '192.168.0.190',
        'NAME': 'fastmeetweb',
        'NAME_TEST': 'fastmeetweb_test',
        'USER': 'fastmeetweb',
        'PASSWORD': 'fastmeetweb',
    }
