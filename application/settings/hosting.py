from .base import BaseSettings
from application.const.peer import DEBUG_NO_LOGS
import logging


class HostingSettings(BaseSettings):
    DEBUG = False
    LOGGING_LEVEL = logging.NOTSET

    # WEB SERVER PARAMS
    SERVER = {
        'HOST': '0.0.0.0',
        'PORT': '80',
    }

    # PEER SERVER PARAMS
    MEETING = {
        'HOST': '45.84.225.128',
        'PATH': '/webrtctunel',
        'SECURE': True,
        'DEBUG': DEBUG_NO_LOGS,
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': '45.84.225.128',
        'NAME': 'fastmeetweb',
        'NAME_TEST': 'fastmeetweb_test',
        'USER': 'fastmeetweb',
        'PASSWORD': 'fastmeetweb',
    }
