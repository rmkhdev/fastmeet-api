import logging
from application.const.peer import DEBUG_ALL


class BaseSettings:
    DEBUG = True
    LOGGING_LEVEL = logging.INFO

    # WEB SERVER PARAMS
    SERVER = {
        'HOST': '127.0.0.1',
        'PORT': '4501',
    }

    # PEER SERVER PARAMS
    MEETING = {
        'HOST': '127.0.0.1',
        'PATH': '/webrtctunel',
        'SECURE': False,
        'DEBUG': DEBUG_ALL,
    }

    # POSTGRES DATABASE
    DATABASE = {
        'HOST': '127.0.0.1',
        'NAME': 'fastmeetweb',
        'NAME_TEST': 'fastmeetweb_test',
        'USER': 'fastmeetweb',
        'PASSWORD': 'fastmeetweb',
    }

    # authentication session params
    AUTH = {
        'COOKIE_NAME': 'FASTMEETAPI_SESSION',
        'PATH': '/',
        'HTTPONLY': True,
        'LIFETIME': 3600 * 24 * 7,
    }
