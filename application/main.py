import logging
from aiohttp import web
from application.utils.routes import routes_init
from application.config import config
from asyncpgsa import pg
from application.utils.common import get_engine_url
from application.utils.authentication import session_init


# event before start app
async def on_startup(app):
    await pg.init(get_engine_url(config.DATABASE))
    app['db_engine'] = pg


# event after start app
async def on_cleanup(app):
    await app['db_engine'].pool.close()


def create_application():
    app = web.Application()

    # init authentication session
    session_init(app)

    # init routes
    routes_init(app)

    # logs
    logging.basicConfig(level=config.LOGGING_LEVEL)

    # events
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)

    # start web-server
    web.run_app(
        app,
        host=config.SERVER['HOST'],
        port=config.SERVER['PORT'],
    )


def create_application_test():
    app = web.Application()

    # init authentication session
    session_init(app)

    # init routes
    routes_init(app)

    # logs
    logging.basicConfig(level=config.LOGGING_LEVEL)

    # events
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)

    # start web-server
    return app


if __name__ == '__main__':
    create_application()
