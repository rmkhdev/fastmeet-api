from marshmallow import Schema, fields


class PortSchema(Schema):
    id = fields.Integer(dump_only=True)
    port = fields.Integer(required=True)
    using = fields.Boolean(required=True)

    class Meta:
        ordered = True


class PortsRequestSchema(Schema):
    using = fields.Boolean()
    order = fields.String()


class PortsUpdateSchema(Schema):
    port = fields.Integer(load_only=True, required=False)
    using = fields.Boolean(load_only=True, required=False)
