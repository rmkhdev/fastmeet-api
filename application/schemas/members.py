from marshmallow import Schema, fields


class MembersSchema(Schema):
    id = fields.Integer(dump_only=True)
    meeting = fields.Method('format_meeting_field')
    user = fields.Method('format_user_field')

    @staticmethod
    def format_user_field(obj):
        if obj['user_id'] is None or obj['user_id'] == 0:
            return None

        return {
            'id': obj['user_id'],
            'name': obj['user_name'],
            'username': obj['user_username'],
        }

    @staticmethod
    def format_meeting_field(obj):
        if obj['meeting_id'] is None or obj['meeting_id'] == 0:
            return None

        return {
            'id': obj['meeting_id'],
            'name': obj['meeting_name'],
        }

    class Meta:
        ordered = True


class MembersCreateSchema(Schema):
    meeting_id = fields.Integer(load_only=True, required=False)
    user_id = fields.Integer(load_only=True, required=False)
