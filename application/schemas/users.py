from marshmallow import Schema, fields, validate


class UsersSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)
    username = fields.String(required=True)
    level = fields.Integer(required=True)

    class Meta:
        ordered = True


class UsersCreateSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=3))
    username = fields.String(required=True, validate=validate.Length(min=3))
    level = fields.Integer(required=True, validate=validate.OneOf([0, 1, 2, 3]))
    password = fields.String(required=True)


class UsersUpdateSchema(Schema):
    name = fields.String(required=False, validate=validate.Length(min=3))
    password = fields.String(required=False)
    level = fields.Integer(required=False, validate=validate.OneOf([0, 1, 2, 3]))
