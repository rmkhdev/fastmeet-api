from marshmallow import Schema, fields, validate


# get
class MeetingSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(dump_only=True)
    port_id = fields.Integer(dump_only=True)
    port = fields.Integer(dump_only=True)
    status = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    creator_id = fields.Integer(dump_only=True)

    class Meta:
        ordered = True


# list
class MeetingsFilterSchema(Schema):
    page = fields.Integer(required=False)
    per_page = fields.Integer(required=False)
    name = fields.String(required=False)
    ports = fields.List(fields.Integer(), required=False)
    status = fields.List(fields.Integer(), required=False)
    creator_id = fields.Integer(required=False)


class MeetingsSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(dump_only=True)
    status = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)

    port = fields.Method('format_port_field')
    creator = fields.Method('format_creator_field')

    @staticmethod
    def format_port_field(obj):
        if obj['port_id'] is None or obj['port_id'] == 0:
            return None

        return {
            'id': int(obj['port_id']),
            'port': int(obj['port_port']),
            'using': int(obj['port_using']),
        }

    @staticmethod
    def format_creator_field(obj):
        if obj['creator_id'] is None or obj['creator_id'] == 0:
            return None

        return {
            'id': obj['creator_id'],
            'name': obj['user_name'],
            'username': obj['user_username'],
        }

    class Meta:
        ordered = True


class MeetingsListSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(dump_only=True)
    status = fields.Integer(dump_only=True)

    class Meta:
        ordered = True


# edit
class MeetingCreateSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=3))
    port_id = fields.Integer(required=True)
    creator_id = fields.Integer(required=True)
    created_at = fields.DateTime(required=True)
    status = fields.Integer(required=True, validate=validate.OneOf([0, 1, 2, 3]))


class MeetingUpdateSchema(Schema):
    name = fields.String(load_only=True, required=False, validate=validate.Length(min=3))
    status = fields.Integer(load_only=True, required=False, validate=validate.Range(min=0, max=3))
