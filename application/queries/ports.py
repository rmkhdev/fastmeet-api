from application.models import PortsTable
from application.const.query import QUERY_UPDATE_RECORD_NO, QUERY_REMOVE_RECORD_NO
from sqlalchemy import asc, desc


async def query_get(connection, id):
    query = PortsTable.select().where(PortsTable.c.id == id)
    return await connection.fetchrow(query)


async def query_select(connection, values):
    order_allow_fields = ['id', 'port', 'using']
    order_field_default = 'id'

    query = PortsTable.select()

    # filter
    if 'using' in values:
        query = query.where(PortsTable.c.using == values.get('using'))

    # order
    if 'order' in values:
        is_desc = values['order'].startswith('-')
        order_field = values['order'].strip('-')

        if order_field not in order_allow_fields:
            order_field = order_field_default

        if is_desc:
            query = query.order_by(desc(order_field))
        else:
            query = query.order_by(asc(order_field))
    else:
        query = query.order_by(asc(order_field_default))

    return await connection.query(query)


async def query_insert(connection, values):
    query = PortsTable.insert().values(values).returning(PortsTable)
    result = await connection.query(query)
    if len(result):
        return result[0]

    return None


async def query_update(connection, id, values):
    query = PortsTable.update().\
        where(PortsTable.c.id == id).\
        values(values)
    result = await connection.execute(query)
    if result == QUERY_UPDATE_RECORD_NO:
        return False

    return True


async def query_remove(connection, id):
    query = PortsTable.delete().where(PortsTable.c.id == id)
    result = await connection.execute(query)
    if result == QUERY_REMOVE_RECORD_NO:
        return False

    return True


async def query_remove_all(connection):
    query = PortsTable.delete()
    result = await connection.execute(query)
    return result
