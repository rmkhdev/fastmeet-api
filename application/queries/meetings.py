from sqlalchemy import select, desc
from application.models import MeetingTable, PortsTable, UsersTable, MembersTable
from application.const.query import QUERY_REMOVE_RECORD_NO, QUERY_UPDATE_RECORD_NO


async def query_select(connection):
    query = MeetingTable.select()
    return await connection.query(query)


async def query_select_list(connection, page=None, per_page=None, name=None, ports=None, status=None, creator_id=None):
    query = select([
        MeetingTable,
        PortsTable.c.port.label('port_port'),
        PortsTable.c.using.label('port_using'),
        UsersTable.c.name.label('user_name'),
        UsersTable.c.username.label('user_username')
    ])\
        .select_from(
            MeetingTable.outerjoin(
                PortsTable, MeetingTable.c.port_id == PortsTable.c.id
            ).outerjoin(
                UsersTable, MeetingTable.c.creator_id == UsersTable.c.id
            )
        )

    # filter - name
    if name is not None and len(name) > 0:
        query = query.where(MeetingTable.c.name.like(f'%{name}%'))

    # filter - creator_id
    if creator_id is not None and creator_id > 0:
        query = query.where(MeetingTable.c.creator_id == creator_id)

    # filter - ports
    if ports is not None and len(ports) > 0:
        query = query.where(MeetingTable.c.port_id.in_(ports))

    # filter - status
    if status is not None and len(status) > 0:
        query = query.where(MeetingTable.c.status.in_(status))

    # pagging
    if page > -1 and per_page > -1:
        query = query.limit(per_page).offset(page * per_page)

    query = query.order_by(desc(MeetingTable.c.id))

    return await connection.fetch(query), await query_count(connection=connection, query=query)


async def query_select_list_by_member(connection, member_id):
    query = select([
        MeetingTable,
    ])
    query = query.where(MembersTable.c.user_id == member_id)
    query = query.where(MembersTable.c.meeting_id == MeetingTable.c.id)
    query = query.order_by(desc(MeetingTable.c.id))

    return await connection.fetch(query)


async def query_count(connection, query):
    query = query.limit(None).offset(None)
    query = query.alias('count_rows').count()

    return await connection.fetchval(query)


async def query_get(connection, id):
    query = MeetingTable.select().where(MeetingTable.c.id == int(id))
    return await connection.fetchrow(query)


async def query_get_ports(connection, id):
    query = select([MeetingTable, PortsTable.c.port])\
        .select_from(MeetingTable.outerjoin(PortsTable, MeetingTable.c.port_id == PortsTable.c.id))\
        .where(MeetingTable.c.id == int(id))
    return await connection.fetchrow(query)


async def query_get_meeting(connection, id):
    query = select([
        MeetingTable,
        PortsTable.c.port.label('port_port'),
        PortsTable.c.using.label('port_using'),
        UsersTable.c.name.label('user_name'),
        UsersTable.c.username.label('user_username')
    ])\
        .select_from(
            MeetingTable.outerjoin(
                PortsTable, MeetingTable.c.port_id == PortsTable.c.id
            ).outerjoin(
                UsersTable, MeetingTable.c.creator_id == UsersTable.c.id
            )
        )\
        .where(MeetingTable.c.id == int(id))

    return await connection.fetchrow(query)


async def query_insert(connection, values):
    query = MeetingTable.insert().values(values).returning(MeetingTable)
    return await connection.query(query)


async def query_update(connection, id, values):
    query = MeetingTable.update().\
        where(MeetingTable.c.id == id).\
        values(values)
    result = await connection.execute(query)
    if result == QUERY_UPDATE_RECORD_NO:
        return False

    return True


async def query_remove(connection, id):
    query = MeetingTable.delete().where(MeetingTable.c.id == id)
    result = await connection.execute(query)
    if result == QUERY_REMOVE_RECORD_NO:
        return False

    return True


async def query_remove_all(connection):
    query = MeetingTable.delete()
    result = await connection.execute(query)
    return result
