from application.models import UsersTable
from application.const.query import QUERY_REMOVE_RECORD_NO, QUERY_UPDATE_RECORD_NO


async def query_get(connection, id):
    query = UsersTable.select().where(UsersTable.c.id == id)
    return await connection.fetchrow(query)


async def query_get_by_username(connection, username):
    query = UsersTable.select().where(UsersTable.c.username == username)
    return await connection.fetchrow(query)


async def query_select(connection):
    query = UsersTable.select()
    return await connection.query(query)


async def query_insert(connection, values):
    query = UsersTable.insert().values(values).returning(UsersTable)
    result = await connection.query(query)
    if len(result):
        return result[0]

    return None


async def query_update(connection, id, values):
    query = UsersTable.update().\
        where(UsersTable.c.id == id).\
        values(values)
    result = await connection.execute(query)
    if result == QUERY_UPDATE_RECORD_NO:
        return False

    return True


async def query_remove(connection, id):
    query = UsersTable.delete().where(UsersTable.c.id == id)
    result = await connection.execute(query)
    if result == QUERY_REMOVE_RECORD_NO:
        return False

    return True


async def query_remove_all(connection):
    query = UsersTable.delete()
    result = await connection.execute(query)
    return result
