from application.models import MembersTable, UsersTable, MeetingTable
from sqlalchemy import select
from application.const.query import QUERY_REMOVE_RECORD_NO


async def query_get(connection, id):
    query = select([
            MembersTable,
            UsersTable.c.name.label('user_name'),
            UsersTable.c.username.label('user_username'),
            MeetingTable.c.name.label('meeting_name'),
    ])\
        .select_from(
            MembersTable.outerjoin(
                UsersTable, UsersTable.c.id == MembersTable.c.user_id
            ).outerjoin(
                MeetingTable, MeetingTable.c.id == MembersTable.c.meeting_id
            )
        ).where(
            MembersTable.c.id == id
        )
    return await connection.fetchrow(query)


async def query_select_by_meeting(connection, meeting):
    query = select([
            MembersTable,
            UsersTable.c.name.label('user_name'),
            UsersTable.c.username.label('user_username'),
            MeetingTable.c.name.label('meeting_name'),
    ])\
        .select_from(
            MembersTable.outerjoin(
                UsersTable, UsersTable.c.id == MembersTable.c.user_id
            ).outerjoin(
                MeetingTable, MeetingTable.c.id == MembersTable.c.meeting_id
            )
        ).where(
            MembersTable.c.meeting_id == meeting
        )
    return await connection.fetch(query)


async def query_select_by_member(connection, member):
    query = select([
            MembersTable,
            UsersTable.c.name.label('user_name'),
            MeetingTable.c.name.label('meeting_name'),
    ])\
        .select_from(
            MembersTable.outerjoin(
                UsersTable, UsersTable.c.id == MembersTable.c.user_id
            ).outerjoin(
                MeetingTable, MeetingTable.c.id == MembersTable.c.meeting_id
            )
        ).where(
            MembersTable.c.user_id == member
        )
    return await connection.fetch(query)


async def query_insert(connection, values):
    query = MembersTable.insert().values(values).returning(MembersTable)
    result = await connection.query(query)
    if len(result):
        return result[0]

    return None


async def query_remove(connection, id):
    query = MembersTable.delete().where(MembersTable.c.id == id)
    result = await connection.execute(query)
    if result == QUERY_REMOVE_RECORD_NO:
        return False

    return True


async def query_remove_meeting(connection, meeting_id, user_id):
    query = MembersTable.delete()\
        .where(MembersTable.c.meeting_id == meeting_id)\
        .where(MembersTable.c.user_id == user_id)

    result = await connection.execute(query)
    if result == QUERY_REMOVE_RECORD_NO:
        return False

    return True
