DROP DATABASE IF EXISTS fastmeetweb;
CREATE DATABASE fastmeetweb;
CREATE USER fastmeetweb  WITH LOGIN password 'fastmeetweb';
GRANT ALL privileges ON DATABASE fastmeetweb TO fastmeetweb;
