#!/bin/sh

##################################################################
# Script for starting application
##################################################################

export PYTHONPATH=$(pwd)
echo $PYTHONPATH

# migrate
echo '';
echo 'Start migrate:';
alembic upgrade head

# application
echo '';
echo 'Start app:';
python application/main.py