FROM python:3.7.1

WORKDIR /app/

COPY Pipfile .
COPY Pipfile.lock .

ENV env 'production'
ENV PYTHONUNBUFFERED 1
RUN pip install pipenv && pipenv install --ignore-pipfile --system

COPY entrypoint.sh .
COPY alembic.ini .
COPY .flake8 .
COPY application application
COPY alembic alembic
